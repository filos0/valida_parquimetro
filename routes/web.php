<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/permisoparquimetro/placa/{md5}', 'ValidaController@valida_placa');

Route::get('/x0x', function (){
    return "LA APLICACIÓN ESTA ACTIVA";
});
