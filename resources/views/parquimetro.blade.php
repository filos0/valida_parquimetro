<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Revista Discapacitados</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">


    <link href="{{ asset('/assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/assets/css/colors.css') }}" rel="stylesheet" type="text/css">


    <!-- Core JS files -->


    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.min.js') }}"></script>


</head>
<body>
<div class="navbar navbar-inverse">
    <div class="navbar-header">
        <a class="navbar-brand">Secretaría de Movilidad</a>

        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
        </ul>
    </div>

</div>
<div class="page-container"  >


    <div class="page-content" style="background-color: white">


        <div class="content-wrapper">


            <div class="content">

                @if(isset($permiso))
                    <div class="content-group" style="height: 85vh">
                        <br>
                        <div class="row">
                            <div class="text-center">
                                <h1 class="text-pink"><b>PERMISO RENOVABLE PARA RESIDENTES VÁLIDO <i
                                                style="font-size: 35px;color: green;" class="icon icon-check2"></i> </b>
                                </h1>
                            </div>
                        </div>
                        <br>
                        <br>

                        <div class="col-md-12">
                            <div class="col-md-4 col-md-offset-2">
                                <br>
                                <br>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <label style="font-size: 22px"><b>FOLIO PERMISO
                                                : </b>{{$permiso->holograma->folio_holograma}}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label style="font-size: 22px"><b>PLACA : </b>{{$permiso->placa}}</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label style="font-size: 22px"><b>VIGENCIA DE PERMISO
                                                : </b>{{date('d-m-Y', strtotime($permiso->holograma->vigencia_documento))}}
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <label style="font-size: 22px"><b>CLAVE DE ZONA
                                                : </b>{{$permiso->propietario->zona->clave}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="text-center">
                                        <label style="font-size: 22px"><b></b></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="text-center">
                                        <img class="img-responsive"
                                             src="{{asset('/poligonos/').'/'.$permiso->propietario->zona->clave}}.png"
                                             alt="">
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                @else
                    <div class="content">


                        <div class="row" style="height: 85vh">


                            <div class="col-md-8">
                                <div class="text-center" style="margin-top: 30%">
                                    <code style="font-size: 40px">PERMISO RENOVABLE</code><br>
                                    <code style="font-size: 40px">PARA RESIDENTES NO VÁLIDO</code>
                                </div>
                            </div>
                            <div class="col-md-4 " style="margin-top: 10%">
                                <img class="img-responsive" src="{{asset('/assets/img/error.jpg')}}" alt="">
                            </div>


                        </div>

                    </div>
                @endif
            </div>

        </div>

    </div>


</div>

</body>
</html>

