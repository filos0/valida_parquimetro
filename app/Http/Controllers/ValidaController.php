<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permiso_Propietario_Modelo as Permiso;

class ValidaController extends Controller
{
    function valida_placa($md5)
    {

        $permiso = Permiso::where('md5', $md5)->get();
        if ($permiso->isEmpty())
            return view('parquimetro');

        return view('parquimetro')->with('permiso', $permiso[0]);
    }
}
