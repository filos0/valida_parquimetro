<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permiso_Propietario_Modelo extends Model
{


    protected $table = 'permiso_propietario';

    protected $fillable = ['fecha_permiso', 'modulo_id', 'users_id', 'solicitante', 'placa', 'propietario_id'];

    protected $primaryKey = 'id_permiso_propietario';

    public $timestamps = false;

    public function holograma()
    {
        return $this->hasOne('App\Models\Holograma_Modelo', 'permiso_propietario_id');
    }

    public function propietario()
    {
        return $this->belongsTo('App\Models\Propietario_Modelo', 'propietario_id');
    }
}
